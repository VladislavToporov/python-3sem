def AtoR(filename):
    list = (int(s) for str in open(filename).readlines() for s in str.split())  # генератор выражений
    # list = range(1, 1000)
    output = open("output.txt", 'w')

    template = ['0', '00', '000', '01', '1', '10', '100', '1000', '02']
    example = ["IVX", "XLC", "CDM", "M"]

    for number in list:
        ans = ''
        k = 0
        while (number > 0):
            digit = number % 10
            template_pnt = template[digit - 1]

            for i in template_pnt[::-1]:
                if digit != 0:
                    ans += example[k][int(i)]
            number //= 10
            k += 1
        output.write(ans[::-1] + " ")
    output.close()


AtoR("input.txt")


def RtoA(filename):
    list = (s for str in open(filename).readlines() for s in str.split())  # генератор выражений
    output = open("output.txt", 'w')

    example = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}

    for number in list:
        ans = 0
        k = 0
        number = number[::-1]
        while k < len(number):
            ans += example[number[k]]
            k += 1
        output.write(str(ans) + " ")
    output.close()

# RtoA("input2.txt")
