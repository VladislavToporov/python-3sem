from functools import reduce


def a(*args):
    s = 0
    for elem in args:
        s += elem
    # s = reduce(lambda x, y : x + y, args)
    # s = sum(args)
    return s


print(a(1, 2, 3))
b = lambda *args: sum(args)
print(b(1, 2, 3))
