def tell_the_date(day, month, year = 2017):
    return ("Today is {day} of month {month} of year {year}".format(day=day, month=month, year=year))

print(tell_the_date(1, 2, 3))
r = {1 : 2, 3 : 4}
for key in r:
    print(r[key])