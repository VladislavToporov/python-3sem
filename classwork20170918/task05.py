import os

while True:
    string = input(os.path.abspath(os.curdir) + "\t")
    command, *other = string.split(" ")
    if command == "cd":
        if len(other) == 1:
            if other[0] == "..":
                os.chdir(os.path.join(os.curdir, os.pardir))
            elif other[0].startwith("./"):
                os.chdir(os.path.join(os.curdir, os.curdir + os.sep + other[0][2:]))
            else:
                os.chdir(os.path.join(os.path.abspath(os.curdir), os.curdir + os.sep + other[0]))
        else:
            SystemError("Something wrong")

    elif command == "ls" or command == "dir":
        for d, dirs, files in os.walk(os.curdir):
            print("\n".join(dirs))

    elif command == "cat":
        if other[0].startswith(">>"):
            f = open(other[0][2:], 'a')
            f.write("\n" + " ".join(other[2:]))
            f.close()
        elif other[0].startswith(">"):
            f = open(other[0][1:], 'w')
            f.write("")
            f.close()
        else:
            print(open(other[0]).read())
    elif command == "exit":
        print("Спасибо, приходите ещё")
        break
    else:
        SystemError("Something wrong")
