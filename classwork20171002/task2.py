def a(filename):
    import re
    string = (s for s in open(filename))
    string = ' '.join(string)
    result = re.findall("[0-1][0-9]:[0-5][0-9]:[0-5][0-9]|2[0-3]:[0-5][0-9]:[0-5][0-9]", string)
    return (result)


def b(filename):
    result = a(filename)
    sum_h = sum_m = sum_s = n = 0
    for str in result:
        n += 1
        h, m, s = str.split(":")
        sum_h += int(h)
        sum_m += int(m)
        sum_s += int(s)
    sum_m += sum_s // 60
    sum_s = sum_s % 60
    sum_h += sum_m // 60
    sum_m = sum_m % 60
    return ("%d:%d:%d" % (sum_h // n, sum_m // n, sum_s // n))


print(a("file1.txt"))
print(b("file1.txt"))
