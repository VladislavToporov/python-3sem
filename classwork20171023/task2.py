import pygame, time
from math import sqrt

from pygame.rect import Rect

pygame.display.init()
M, N = 800, 800
screen = pygame.display.set_mode((M, N))
r = 50
x0 = 400
y0 = 400
color = (255, 255, 0)
pygame.display.set_caption('test')
delta = 1

N = 10
counter = 0
begin = time.time()
file = open("file.txt", "a")
go = True
change_size_time = time.time()

r_min = 10
r_max = 100
while(go):
    screen.fill((0,0,0))
    screen.lock()

    """
    for x in range(x0 - r, x0 + r + 1):
        y = round(sqrt(r ** 2 - (x - x0) ** 2) + y0)
        screen.set_at((x, y), color)
        y = round(-sqrt(r ** 2 - (x - x0) ** 2) + y0)
        screen.set_at((x, y), color)
    """
    #pygame.draw.circle(screen, (255, 255, 0), (x0, y0), r, 10)

    #pygame.draw.rect(screen, (255, 255, 0), (x0, y0, x0 + 100, y0 + 100), 0)
    screen.unlock()
    pygame.display.flip()
    counter += 1
    if (time.time() - begin > 1):
        file.write(str(counter) + "\n")
        print(counter)
        begin = time.time()
        counter = 0
    pressed_list = pygame.key.get_pressed()
    pygame.event.pump()

    if (pygame.key.get_pressed()[pygame.K_ESCAPE]):
        go = False

    if (pygame.key.get_pressed()[pygame.K_UP]):
        y0 -= 1

    if (pygame.key.get_pressed()[pygame.K_DOWN]):
        y0 += 1

    if (pygame.key.get_pressed()[pygame.K_LEFT]):
        x0 -= 1

    if (pygame.key.get_pressed()[pygame.K_RIGHT]):
        x0 += 1


    if (time.time() - change_size_time > 0.01):
        r += delta

    if (r == r_max):
        delta = -1
    elif (r == r_min):
        delta = 1

file.close()
pygame.display.quit()
