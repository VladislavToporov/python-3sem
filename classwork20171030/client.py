import socket

HOST = 'localhost'
PORT = 1234
s = socket.socket()
s.connect((HOST, PORT))

message = b"H"
s.sendall(message)
data = s.recv(len(message))
print(str(data.decode("utf-8")))
s.close()
