import socket

HOST = ''
PORT = 1234
s = socket.socket()
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
message = b"0"
data = conn.recv(len(message))
print(str(data.decode("utf-8")))
conn.sendall(message)
