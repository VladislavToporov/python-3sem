import random
import threading

import time


def printNum1(n, name, e1, e2):
    for i in range(n):
        e1.wait()
        r = random.randint(0, 100)
        #print(random.randint(0, 100), name)
        e1.clear() # заснуть
        e2.set() # разбудить

e1 = threading.Event()
e2 = threading.Event()
t1 = threading.Thread(target=printNum1, args=(10, "th1", e1, e2))
t1.daemon = True


t2 = threading.Thread(target=printNum1, args=(10, "th2", e2, e1))
t2.daemon = True

d = time.time()
t1.start()
t2.start()
e1.set()
t1.join()
t2.join()
print(time.time() - d)

d = time.time()
printNum1(20, "SEC", e1, e1)
print(time.time() - d)