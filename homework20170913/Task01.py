def AtoR(filename):
    lst = (int(s) for string in open(filename).readlines() for s in string.split())
    output = open("output.txt", 'w')

    template = ['0', '00', '000', '01', '1', '10', '100', '1000', '02']
    example = ["IVX", "XLC", "CDM", "M"]

    for number in lst:
        ans = ''
        k = 0
        while number > 0:
            digit = number % 10
            template_pnt = template[digit - 1]

            for i in template_pnt[::-1]:
                if digit != 0:
                    ans += example[k][int(i)]
            number //= 10
            k += 1
        output.write(ans[::-1] + " ")
    output.close()


if __name__ == "__main__":
    AtoR("input.txt")
