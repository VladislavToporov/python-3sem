from math import sqrt


class Vector2D:
    def __init__(self, x=0, y=0) -> None:
        self.x = x
        self.y = y

    def __add__(self, v2):
        return Vector2D(self.x + v2.x, self.y + v2.y)

    def __sub__(self, v2):
        return Vector2D(self.x - v2.x, self.y - v2.y)

    def __mul__(self, other):
        if str(other).isdigit():
            return Vector2D(self.x * other, self.y * other)
        return self.x * v2.x + self.y * v2.y

    def __str__(self) -> str:
        return "{%s; %s}" % (self.x, self.y)

    def length(self) -> float:
        return sqrt(self.x ** 2 + self.y ** 2)

    def abs_vector2d(self):
        return self.length()

    def __eq__(self, v2) -> bool:
        return self.x == v2.x and self.y == v2.y


if __name__ == "__main__":
    v1 = Vector2D(1, 2)
    v2 = Vector2D(1, 2)
    print(v1 + v2)
    print(v1 - v2)
    print(v1 * 2)
    print(v1 * v2)
    print(v1 == v2)
    print(v1.length())
