import math


class RationalFraction:
    def __init__(self, x=0, y=1) -> None:
        self.x = x
        self.y = y

    def reduce(self):
        if self.y < 0 and self.x < 0 or self.y < 0 and self.x > 0:
            self.y *= -1
            self.x *= -1

        minimum = abs(self.x) if abs(self.x) < abs(self.y) else abs(self.y)

        for i in range(minimum, 1, -1):
            while self.x % i == 0 and self.y % i == 0:
                self.x //= i
                self.y //= i

        return self

    def __add__(self, rf2):
        return RationalFraction(self.x * rf2.y + rf2.x * self.y, self.y * rf2.y).reduce()

    def __sub__(self, rf2):
        return RationalFraction(self.x * rf2.y - rf2.x * self.y, self.y * rf2.y).reduce()

    def __mul__(self, rf2):
        return RationalFraction(self.x * rf2.x, self.y * rf2.y).reduce()

    def __floordiv__(self, rf2):
        return RationalFraction(self.x * rf2.y, self.y * rf2.x).reduce()

    def __str__(self) -> str:
        self.reduce()
        if abs(self.x) == abs(self.y) == 1:
            return str(self.x)
        elif self.x == 0:
            return "0"
        return "(%s / %s)" % (self.x, self.y)

    def __eq__(self, rf2) -> bool:
        return self.reduce().x == rf2.reduce().x and self.reduce().y == rf2.reduce().y

    def value(self) -> float:
        return self.x / self.y

    def numberPart(self) -> int:
        return int(self.x / self.y)


if __name__ == "__main__":
    v1 = RationalFraction(-1, 3)
    v2 = RationalFraction(1, 2)
    print(v1 + v2)
    print(v1 - v2)
    print(v1 * v2)
    print(v1 // v2)
    print(v1.numberPart())
    print(v1.value())
    print(v1 == v2)
