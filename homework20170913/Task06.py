import re

# generator expressions
lst = (re.sub(r'[\W\d]', '', s.lower()) for s in open('input06.txt').read())
dic = {}
for letter in lst:
    dic[letter] = dic.get(letter, 0) + 1
print(dic)
