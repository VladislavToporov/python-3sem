import requests
from bs4 import BeautifulSoup


def final_render(container, flag1=False, flag2=False):
    begin = "*  " if flag2 else ""
    end = "\t" if flag1 else "\n"

    try:
        print(begin + str(container.get_text()).strip(), end=end)
    except:
        print(begin + str(container.zfill(20, "") if flag1 else container).strip(), end=end)


def render(container):
    children = list(container.children)
    if len(children) > 0:
        for elem in children:
            if elem == "\n":
                children.remove(elem)
        for i in range(len(children)):
            name1 = children[i].name
            name2 = children[i + 1].name if i + 1 < len(children) else ""

            flag1 = name1 == name2 and name2 == "td"
            flag2 = name1 == "li"
            if flag1 or flag2:
                final_render(children[i], flag1, flag2)
            elif name1 != None:
                render(children[i])
            else:
                final_render(children[i], flag1, flag2)


page = requests.get("http://localhost:63342/pythononly/classwork20171120/site.html?_ijt=h6ogu9dusc5j3d0iendcpre6jd")
soup = BeautifulSoup(page.content, "html.parser")
html = list(soup.children)[2]
body = list(html.children)[3]
render(body)
