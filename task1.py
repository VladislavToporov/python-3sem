from threading import Thread

import time


def func(n):
    for i in range(n):
        x = (i)

begin = time.time()
t1 = Thread(target=func, args=(100000,))
t1.daemon = True
t1.start()
t1.join()

print("time", time.time() - begin)

begin = time.time()

func(100000)

print("time", time.time() - begin)