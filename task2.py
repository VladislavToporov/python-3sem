from multiprocessing import Process, freeze_support

import time


def func(n):
    for i in range(n):
        x = (i)


if __name__ == '__main__':
    freeze_support()

    begin = time.time()
    t1 = Process(target=func, args=(100000,))
    t1.daemon = True
    t1.start()
    t1.join()

    print("time", time.time() - begin)

    begin = time.time()
    func(100000)
    print("time", time.time() - begin)

