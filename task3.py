import random
from threading import Thread
from threading import Event

import time


def func(n, name, e1, e2):
    for i in range(n):
        e1.wait()
        print(name, random.randint(0, n))
        e1.clear()
        e2.set()

begin = time.time()

e1 = Event()
e2 = Event()
t1 = Thread(target=func, args=(10, "t1", e1, e2))
t1.daemon = True

t2 = Thread(target=func, args=(10, "t2", e2, e1))
t2.daemon = True

e1.set()
t1.start()
t2.start()


t1.join()
t2.join()
