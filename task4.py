import random
from multiprocessing import Process, freeze_support, Queue
from multiprocessing import Array

import time


def func(n, lst):
    for i in range(n):
        x = random.randint(0, n)
        lst.put(x)
        print(x, end="")


if __name__ == '__main__':
    freeze_support()
    n = 10
    lst = Queue()
    t1 = Process(target=func, args=(n, lst))
    t1.daemon = True

    t1.start()
    t1.join()
    print()
    while not lst.empty():
        print(lst.get(), end="")
